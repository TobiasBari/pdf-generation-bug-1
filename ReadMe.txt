1. Start the Web project first so the the web server is running. 
It is needed for the Console Test project. 
	Explanation of why:
	The console project uses the main HTML file locally in most tests, 
	but all the CSS and js references in the HTML are fetched from the web server project. 
	So it needs to be started

2. Run the Console test project (which uses the web project). 
The output will be placed the TextX_xxx folder in this folder dependent on which test you run.



