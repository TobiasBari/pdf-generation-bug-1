﻿using Console_TestABCpdfProblem.Tools;

namespace Console_TestABCpdfProblem
{
    class Program
    {
        static void Main()
        {
            #region Documentation of test history
            /* This test Console project is an exploratory way to test diffent scenarios of getting the ABCpdf component to work generating an equivalent PDF file from an HTML file
             * that has dynamic content adjustment of the HTML in hte client after browser load.
             * Each test case is explained below
             *
             * --- Test1_AddImageHtml_BaselineAsInProductionCausingTheProblem ------------------------------------
             * This test is using a file based HTML file in this project and using the production way of generating the print view of the HTML file to a PDF. However, all CSS and JavaScript references are still fetched from the web server.
             * It is NOT working. the ABCpdf function AddImageHtml does not handle the referenced Javascript making the DOM state transitition correctly before the PDF is generated.
             *
             *
             * --- Test2_AddImageHtml_InlineReferences ------------------------------------
             * In this test, all CSS and Javascript references in the HTML file are fetched and instead inlined in the HTML file.
             * It is NOT working.
             *
             *
             * --- Test3_AddImageUrl ------------------------------------
             * Here, the HTML page is instead featched directly from the web server and the ABCpdf function AddImageUrl is used instead.
             * It works.
             * However, there's a problem.
             * In the real production scenario, the HTML call needs to be authenticated using a HTTP header parameter with a JWT token.
             * ABCpdf component does not allow this to be set for the most modern Chrome engine (that the ABCpdf component is using).
             * TODO: Check if the older browser engines are ok to use for the end result. The HTML is not so complicated so perhaps this will be good enough.
             *
             * --- Test4_AddImageUrl_UsingFileReference ------------------------------------
             * In this solution, the local HTML is referenced using the AddImageUrl function with a file:/// reference to a local path on the server instead of the Web Server.
             * In the real prod scenario, this version would mean that the HTML is first persisted to file by calling the web server.
             * It works.
             *
             *
             *
             * ------------- NOTE: Test3 and Test3 both solves the main problem PDF creation problem. -------------
             * But there is one race-condition problem remainging for both of these solutions.
             * In order for the PDF to render correctly, a ABCpdf provided function RepaintDelay has to be set to at least 5 seconds.
             * This is due to that (locally on my developer machine),
             * it takes a few seconds before the HTML DOM transition is completed rendering the correct state of the HTML.
             * This means that if the server is busy in rare cases, there is a race condition that may render the incorrect state in the PDF,
             * which would render a hard to find intermittently occuring error in the PDF. This is not acceptable in production.
             *
             * PROPER SOLUTION:
             * The proper solution would instead be to let the PDF creation be triggered in the ABCpdf component WHEN the state HTML DOM state transition is complete.
             * However, since the state transition is obtained by a third party, Episerver, it is tricky to obtain a robust long term solution
             * that is not subject to changes of future versions of Episerver.
             *
             */

            #endregion

            /* IMPORTANT: The web server Web_testAbcPdfProblem needs to be started for all of the tests to work. It also resides in the GIT repo.
             Even though the HTML page that is used resides in this project, all the external resources (css and javascript) it references resides on the web server instead (which mimics the reousrce that Episerver gives to the page)
             */

            var test = new TestExecutor();
            #region NOTE: Previous archived test versions, not valid any longer since they depend upon a CreatePdf method that has been altered (no point in making this design generic enough at this point
            //test.Test1_AddImageHtml_BaselineAsInProductionCausingTheProblem();
            //test.Test2_AddImageHtml_InlineReferences();
            //test.Test3_AddImageUrl();
            //test.Test4_AddImageUrl_UsingFileReference();
            #endregion
            
            test.Test5_AddImageUrl_AddingAbcPdfJavaScript();
        }
    }
}
