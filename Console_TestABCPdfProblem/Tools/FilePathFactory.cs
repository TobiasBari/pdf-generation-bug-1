﻿using System;
using System.IO;

namespace Console_TestABCpdfProblem.Tools
{
    public class FilePathFactory
    {
        private readonly string _outputDir;
        private readonly string _outputFileBaseName;
        private readonly string _assemblyDirectory;

        public FilePathFactory(string relativeOutputPath, string relativeInputPath=null)
        {
            Console.WriteLine("Starting test: " + relativeOutputPath);
            _assemblyDirectory = AppDomain.CurrentDomain.BaseDirectory;
            _outputDir = _assemblyDirectory + @"..\..\..\" + relativeOutputPath;
            if (!Directory.Exists(_outputDir)) Directory.CreateDirectory(_outputDir);
            _outputFileBaseName = $"index_{DateTime.Now:yyyyMMdd-HH_mm_ss}";

            if (relativeInputPath == null) return;
            InputHtmlFile = _assemblyDirectory + relativeInputPath;
        }

        public string InputHtmlFile { get; }

        public string InputHtmlFileLocalUrl => "file:///" + InputHtmlFile;

        public string OutputHtmlPath => Path.Combine(_outputDir, _outputFileBaseName + ".htm");
        public string OutputPdfPath => Path.Combine(_outputDir, _outputFileBaseName + ".pdf");

        public string OutputFilePath => _outputDir;

    }
}
