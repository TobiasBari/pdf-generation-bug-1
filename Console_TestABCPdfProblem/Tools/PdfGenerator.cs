﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using WebSupergoo.ABCpdf11;

namespace Console_TestABCpdfProblem.Tools
{
    public class PdfGenerator
    {
        private static int _secondsElapsed = 0;
        public static void CreatePdfUsingAddImageHtml(string html, string filePath)
        {
            MonitorTimeOfExecution(() => CreatePdf((doc) => doc.AddImageHtml(html), filePath));
        }


        public static void CreatePdfUsingAddImageUrl(string url, string filePath)
        {
            MonitorTimeOfExecution(() => CreatePdf((doc) => doc.AddImageUrl(url), filePath));
        }

        private static void MonitorTimeOfExecution(Action methodToMonitor)
        {
            _secondsElapsed = 0;
            var start = DateTime.Now;
            var pdfTask = Task.Run(methodToMonitor);
            while (pdfTask.Status != TaskStatus.RanToCompletion)
            {
                Console.WriteLine(++_secondsElapsed + " ");
                pdfTask.Wait(1000);
            }
            Console.WriteLine($"Timetaken: {(DateTime.Now - start).Seconds} seconds.");
        }

        public async Task CreatePdfUsingAddImageUrlAsync(string url, string filePath)
        {
            int TIME_TO_WAIT_BERFORE_ABORTING_THREAD = 10000;

            var pdfTask = Task.Factory.StartNew(() => CreatePdf((doc) => doc.AddImageUrl(url), filePath));
            var start = DateTime.Now;
            var message = string.Empty;
            var useThreadTimer = true;
            if (useThreadTimer)
            {
                message = await Task.WhenAny(pdfTask, Task.Delay(TIME_TO_WAIT_BERFORE_ABORTING_THREAD)) == pdfTask
                    ? $"Success! Thread complete timetaken: {(DateTime.Now - start).Milliseconds} milliseconds."
                    : "Failure after 10 seconds.";
            }
            else
            {
                //if this scenario is configured, it works if:
                //--> 1. change VERY_LONG_TIME_THAT_NEVER_WILL_BE_REACHED to 2000 (2 seconds) insted of 20000 (20 seconds) 
                //--> 2. comment out the  ABCChromeExt.Render() in the index_AbcPdfJsAdded.htm file.
                //But there is no guarantee in this scenario that the PDF is created with correct information since the AbcPDF component only assumes it is correct.
                await pdfTask;
                message = $"Thread completed.  timetaken: {(DateTime.Now - start).Milliseconds} milliseconds.";
            }
            Console.WriteLine("--> " + message);
        }

        private static void CreatePdf(Func<Doc, int> pdfAddImageDelegate, string filePath)
        {
            int VERY_LONG_TIME_THAT_NEVER_WILL_BE_REACHED = 20000; //The Task (thread) timout is 10 seconds so will be reached earlier than this.
            using (Doc doc = new Doc())
            {
                // Set HTML options
                DateTime start = DateTime.Now;
                doc.HtmlOptions.ForChrome.RepaintDelay = VERY_LONG_TIME_THAT_NEVER_WILL_BE_REACHED; //https://www.websupergoo.com/helppdfnet/default.htm?page=source%2F5-abcpdf%2Fxhtmloptions%2F2-properties%2Frepaintdelay.htm
                doc.HtmlOptions.Media = MediaType.Print;
                doc.HtmlOptions.Engine = EngineType.Chrome;
                doc.HtmlOptions.ForChrome.RepaintTimeout = VERY_LONG_TIME_THAT_NEVER_WILL_BE_REACHED; //https://www.websupergoo.com/helppdfnet/default.htm?page=source%2F5-abcpdf%2Fxhtmloptions%2F2-properties%2Frepainttimeout.htm
                doc.HtmlOptions.UseScript = true;
                
                // Convert HTML
                Console.WriteLine($"PDF generation: doc.AddImageXXX started...");
                int htmlID = pdfAddImageDelegate(doc);
                while (true)
                {
                    Console.WriteLine($"PDF generation: adding image to chain. HtmlId: {htmlID}");
                    if (!doc.Chainable(htmlID)) break;
                    doc.Page = doc.AddPage();
                    htmlID = doc.AddImageToChain(htmlID);
                }

                // Save
                for (int i = 1; i <= doc.PageCount; i++)
                {
                    doc.PageNumber = i;
                    doc.Flatten();
                }
                Console.WriteLine("PDF generation: saving the doc...");
                doc.Save(filePath);
                Console.WriteLine($"--> Done. PDF creation timetaken: {(DateTime.Now - start).Milliseconds} milliseconds.");
            }
        }
    }
}
