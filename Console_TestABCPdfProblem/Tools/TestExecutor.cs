﻿using System;
using System.IO;
using System.Reflection;

namespace Console_TestABCpdfProblem.Tools
{
    public class TestExecutor
    {
        #region Archived test versions
        public void Test1_AddImageHtml_BaselineAsInProductionCausingTheProblem()
        {
            var fileFactory = new FilePathFactory(
                relativeOutputPath: MethodBase.GetCurrentMethod().Name,
                relativeInputPath: "Input/index.htm");

            Console.WriteLine("1. Reading HTML from file.");
            //NOTE: this is a simplification to the real PROD scearion in which the HTML page instead is fetched from the web server providing the necessary parameter to authenticate the user. Here, the HTML file is just loaded from file instead locally in this test console application.
            var html = File.ReadAllText(fileFactory.InputHtmlFile);

            Console.WriteLine("2. Writing converted HTML file to output path (for comparison purpose PDF file).");
            File.WriteAllText(fileFactory.InputHtmlFile, html);

            Console.WriteLine("3. Creating PDF and write to output path...");
            PdfGenerator.CreatePdfUsingAddImageHtml(html, fileFactory.OutputPdfPath);

            WriteDone(fileFactory);
        }

        public void Test2_AddImageHtml_InlineReferences()
        {
            var fileFactory = new FilePathFactory(
                relativeOutputPath: MethodBase.GetCurrentMethod().Name, //old async way: new StackTrace().GetFrame(2).GetMethod().Name,
                relativeInputPath: "Input/index.htm");

            Console.WriteLine("1. Reading HTML from file.");
            var html = File.ReadAllText(fileFactory.InputHtmlFile);

            Console.WriteLine("2. Inlining problematic JavaScripts.");
            var factory = new HtmlConverterFactory(html);
            html = factory.InLineResources()
                .GetAwaiter().GetResult(); ;

            Console.WriteLine("3. Writing converted HTML file to output path (for comparison purpose PDF file).");
            File.WriteAllText(fileFactory.OutputHtmlPath, html);

            Console.WriteLine("4. Creating PDF and write to output path...");
            PdfGenerator.CreatePdfUsingAddImageHtml(html, fileFactory.OutputPdfPath);

            WriteDone(fileFactory);
        }

        public void Test3_AddImageUrl()
        {
            var fileFactory = new FilePathFactory(
                relativeOutputPath: MethodBase.GetCurrentMethod().Name);

            Console.WriteLine("1. Creating PDF and write to output path...");
            PdfGenerator.CreatePdfUsingAddImageUrl(
                "http://localhost:3424/index.htm",
                fileFactory.OutputPdfPath);

            WriteDone(fileFactory);
        }

        public void Test4_AddImageUrl_UsingFileReference()
        {
            var fileFactory = new FilePathFactory(
                relativeOutputPath: MethodBase.GetCurrentMethod().Name,
                relativeInputPath: "Input/Index.htm");
                //relativeInputPath: "Input/Index_withReactRendering.htm");


            Console.WriteLine("1. Creating PDF and write to output path...");
            PdfGenerator.CreatePdfUsingAddImageUrl(
                "file:///" + fileFactory.InputHtmlFile,
                fileFactory.OutputPdfPath);

            WriteDone(fileFactory);
        }


        #endregion

        public void Test5_AddImageUrl_AddingAbcPdfJavaScript()
        {
            var fileFactory = new FilePathFactory(
                relativeOutputPath: MethodBase.GetCurrentMethod().Name,
                relativeInputPath: "Input/index_AbcPdfJsAdded.htm");

            Console.WriteLine("Creating PDF and write to output path...");
            var pdfGenerator = new PdfGenerator();
            pdfGenerator.CreatePdfUsingAddImageUrlAsync(
                "file:///" + fileFactory.InputHtmlFile,
                fileFactory.OutputPdfPath).Wait();

            WriteDone(fileFactory);
        }

        private void WriteDone(FilePathFactory fileFactory)
        {
            Console.WriteLine("Done!");
            Console.WriteLine("Result output path:");
            Console.WriteLine(fileFactory.OutputFilePath);
            Console.WriteLine("Type any key to terminate application.");
            Console.ReadLine();
        }
    }
}
