﻿using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Console_TestABCpdfProblem.Tools
{
    public class HtmlConverterFactory
    {
        private string _html;

        public HtmlConverterFactory(string html)
        {
            _html = html;
        }

        public async Task<string> InLineResources()
        {
            var regExOptions = RegexOptions.Compiled | RegexOptions.IgnoreCase;

            const string javaScriptInlineTemplate = @"<script type=""text/javascript"">{0}</script>";
            //NOTE: will inline the following three Javascripts:
            //1. <script type="text/javascript" src="http://localhost:3424/static/WebResource_Jquery_3.5.1.js"></script>
            //2. <script type="text/javascript" src="http://localhost:3424/static/FormInitScript.js"></script>
            //3. <script type="text/javascript" src="http://localhost:3424/static/WebSource_TzRGl.js"></script>
            await Replace(
                new Regex(@"<script type=""text/javascript"" src=""(?<url>\S+)""></script>", regExOptions),
                javaScriptInlineTemplate);

            //NOTE: The 4th React JavaScript reference on the html page is too big to inline. It will cause the ABCpdf component to time out. However it works to not inline it since the content is rendered as it should in the PDF.
            //4. <script src="http://statsbidrag/static/bundle.client.js"></script>
            //await Replace(
            //    new Regex(@"<script src=""(?<url>\S+)""></script>", regExOptions),
            //    javaScriptInlineTemplate);


            const string cssInlineTemplate = @"<style>{0}</style>";
            //inlining CSS as well (not that it seems to be needed since CSS is working without inlining).
            //1. <link href="http://localhost:3424/static/client.style.css" rel="stylesheet" />
            //2. 
            await Replace(
                new Regex(@"<link href=""(?<url>\S+)"" rel=""stylesheet"" />", regExOptions),
                cssInlineTemplate);

            //2. <link rel='stylesheet' type='text/css' data-f-resource='EPiServerForms.css' href='http://localhost:3424/static/WebResourceA_EPiServerForms.css' />
            await Replace(
                new Regex(@"<link rel='stylesheet' type='text/css' data-f-resource='EPiServerForms\.css' href='(?<url>\S+)'>", regExOptions),
                cssInlineTemplate);

            return _html;

            async Task Replace(Regex regex, string ouputTemplate)
            {
                var matches = regex.Matches(_html);
                foreach (Match match in matches)
                {
                    var reference = match.Value;
                    var url = match.Groups["url"].Value;
                    string content;
                    using (var client = new HttpClient())
                    {
                        content = await client.GetStringAsync(url);
                    }

                    var newValue = string.Format(ouputTemplate, content);
                    _html = _html.Replace(reference, newValue);
                }
            }
        }
    }
}
