﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web.Http;
using WebSupergoo.ABCpdf11;

namespace Web_testAbcPdfProblem.Controllers
{
    public class TestAbcPdfController : ApiController
    {
        // GET: api/TestAbcPdf
        public string Get()
        {
            var webLocalDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string inputHtmlFile = webLocalDirectory + "index.htm";
            var html = File.ReadAllText(inputHtmlFile);

            //NOTE: Above is simulating the extraction of the HTML content to be rendered to a PDF. It is done differently on the actual web server, but is not important from the below tested solution to the main problem. 


            string outputDir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent?.FullName + @"\PdfOutput\";
            string outputFile = $"index_{DateTime.Now:yyyyMMdd-HH_mm_ss}.pdf";

            using (Doc doc = new Doc())
            {
                doc.HtmlOptions.Media = MediaType.Print;
                doc.HtmlOptions.Engine = EngineType.Chrome;
                doc.HtmlOptions.UseProxyServer = true;
                doc.HtmlOptions.PageCacheEnabled = false;
                doc.HtmlOptions.ForChrome.RepaintDelay = 5000;
                doc.HtmlOptions.ForChrome.RepaintTimeout = 60000;

                var id = doc.AddImageHtml(html);

                while (doc.Chainable(id))
                {
                    doc.Page = doc.AddPage();
                    id = doc.AddImageToChain(id);
                }

                if (File.Exists(outputFile)) File.Delete(outputFile);
                doc.Save(Path.Combine(outputDir, outputFile));
            }

            return $"Generated {outputFile}. Located in {outputDir}.";
        }

        //public string Get_UsingUrl()
        //{
        //    string outputDir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent?.FullName + @"\PdfOutput\";
        //    string url = "localhost:3424";
        //    string outputFile = $"index_{DateTime.Now:yyyyMMdd-HH_mm_ss}.pdf";

        //    using (Doc doc = new Doc())
        //    {
        //        doc.HtmlOptions.Media = MediaType.Print;
        //        doc.HtmlOptions.Engine = EngineType.Chrome;
        //        doc.HtmlOptions.UseProxyServer = true;
        //        doc.HtmlOptions.PageCacheEnabled = false;
        //        doc.HtmlOptions.ForChrome.RepaintDelay = 2000;
        //        doc.HtmlOptions.ForChrome.RepaintTimeout = 15000;

        //        var id = doc.AddImageUrl(url);

        //        while (doc.Chainable(id))
        //        {
        //            doc.Page = doc.AddPage();
        //            id = doc.AddImageToChain(id);
        //        }

        //        if (File.Exists(outputFile)) File.Delete(outputFile);
        //        doc.Save(Path.Combine(outputDir, outputFile));
        //    }

        //    return $"Generated {outputFile}. Located in {outputDir}.";
        //}
    }
}
