﻿

/* This view acts as a rendering template to render InitScript(and server-side Form's descriptor) in FormContainerBlock's client-side for Form[1603cf26-05bf-4dc2-ba1c-337ee90ecfc6].
TECHNOTE: all serverside (paths, dynamic values) of EPiServerForms will be transfered to client side here in this section. */
(function initializeOnRenderingFormDescriptor() {
    // each workingFormInfo is store inside epi.EPiServer.Forms, lookup by its FormGuid
    var workingFormInfo = epi.EPiServer.Forms["1603cf26-05bf-4dc2-ba1c-337ee90ecfc6"] = {
        Id: "1603cf26-05bf-4dc2-ba1c-337ee90ecfc6",
        Name: "tofj PDF bug analys form",
        // whether this Form can be submitted which relates to the visitor's data (cookie, identity) and Form's settings (AllowAnonymous, AllowXXX)

        SubmittableStatus: { "submittable": true, "message": "" },
        ConfirmMessage: "",
        ShowNavigationBar: true,
        ShowSummarizedData: false,

        // serialize the dependency configuration of this form to clientside
        DependenciesInfo: [{ "fieldName": "__field_18833", "action": { "displayName": "Visas", "name": "EPiServer.Forms.Core.Internal.Dependency.ShowAction", "order": 1, "clientsideAction": "show" }, "conditionCombination": "All", "conditions": [{ "fieldName": "__field_18832", "operator": "Equals", "fieldValue": "A" }] }],
        // keep all fieldName which are not satisfied the field dependency conditions
        DependencyInactiveElements: [],

        // Validation info, for executing validating on client side
        ValidationInfo: [],
        // Steps information for driving multiple-step Forms.
        StepsInfo: {
            Steps: [{ "index": 0, "attachedUrl": "", "dependField": null, "dependCondition": null, "isActive": true, "attachedContentLink": "", "dependValue": "", "elementName": "__field_", "guid": "00000000-0000-0000-0000-000000000000" }]
        },
        FieldsExcludedInSubmissionSummary: [],
        ElementsInfo: { "__field_18832": { "type": "EPiServer.Forms.Implementation.Elements.ChoiceElementBlock", "friendlyName": "Choice 1", "customBinding": false }, "__field_18833": { "type": "EPiServer.Forms.Implementation.Elements.ChoiceElementBlock", "friendlyName": "Choice 2", "customBinding": false }, "__field_18835": { "type": "Socialstyrelsen.Models.Forms.ContactDetailsBlock", "friendlyName": "Kontaktuppgifter", "customBinding": false } },
        DataSubmitController: "/SbgEpiForms"

    };

    /// TECHNOTE: Calculation at FormInfo level, and these values will be static input for later processing.
    workingFormInfo.StepsInfo.FormHasNoStep_VirtualStepCreated = true;  // this FLAG will be true, if Editor does not put any FormStep. Engine will create a virtual step, with empty GUID
    workingFormInfo.StepsInfo.FormHasNothing = false;  // this FLAG will be true if FormContainer has no element at all
    workingFormInfo.StepsInfo.AllStepsAreNotLinked = true;  // this FLAG will be true, if all steps all have contentLink=="" (emptyString)
})();